<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Void_;

class MahasiswaController extends Controller

{
    
    public function mahasiswa(){
        $hasil = DB::table('mahasiswa')->get();

        return view('mahasiswa',['data_mhs'=>$hasil]);
        $this->middleware('auth');
        
    }

    public function prosesSimpan(Request $req){
        $nama = $req->nama_mhs;
        $nim = $req->nim_mhs;
        $email = $req->email_mhs;
        $notelp = $req->notelp_mhs;
        $prodi = $req->prodi_mhs;
        $jurusan = $req->jurusan_mhs;
        $fakultas = $req->fakultas_mhs;
        
        DB::table('mahasiswa')->insert(
            ['nama'=>$nama,
            'nim'=>$nim,
            'email'=>$email,
            'no_telp'=>$notelp,
            'prodi'=>$prodi,
            'jurusan'=>$jurusan,
            'fakultas'=>$fakultas]
        );
        return redirect('/mahasiswa');
    }
    public function edit($id){
        $editData = DB::table('mahasiswa')->where('id',$id)->first();
        return view('editMahasiswa',compact('editData'));
    }


public function update(Request $req){
    DB::table('mahasiswa')->where('id',$req['id'])->update(
        ['nama'=>$req['nama_mhs'],
        'nim'=>$req['nim_mhs'],
        'email'=>$req['email_mhs'],
        'no_telp'=>$req['notelp_mhs'],
        'prodi'=>$req['prodi_mhs'],
        'jurusan'=>$req['jurusan_mhs'],
        'fakultas'=>$req['fakultas_mhs'],
        
        ]
    );
    return redirect('/mahasiswa');
}

public function deleteData($id){
    DB::table('mahasiswa')->where('id', $id)->delete();
    return redirect('/mahasiswa');
}



}
