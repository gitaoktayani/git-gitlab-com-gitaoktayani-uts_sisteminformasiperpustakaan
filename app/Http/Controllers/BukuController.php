<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class BukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function buku(){
        $hasil = DB::table('buku')->get();

        return view('buku',['data_buku'=>$hasil]);
        
        
    }
    public function prosesSimpanBuku(Request $req){
        $judul = $req->jdl_buku;
        $pengarang = $req->pengarang_buku;
        $penerbit = $req->penerbit_buku;
        $tahun = $req->thn_terbit;
        $tebal = $req->tbl_buku;
        $isbn = $req->isbn_buku;
        $stok = $req->stok_buku;
        $biaya = $req->biaya_buku;
        
        DB::table('buku')->insert(
            ['judul_buku'=>$judul,
            'pengarang'=>$pengarang,
            'penerbit'=>$penerbit,
            'tahun_terbit'=>$tahun,
            'tebal'=>$tebal,
            'isbn'=>$isbn,
            'stok_buku'=>$stok,
            'biaya_sewa_harian'=>$biaya]
        );
        return redirect('/buku');

        
    }
    public function editbuku($id){
        $editData = DB::table('buku')->where('id',$id)->first();
        return view('editBuku',compact('editData'));
    }


public function updatebuku(Request $req){
    DB::table('buku')->where('id',$req['id'])->update(
        ['judul_buku'=>$req['jdl_buku'],
        'pengarang'=>$req['pengarang_buku'],
        'penerbit'=>$req['penerbit_buku'],
        'tahun_terbit'=>$req['thn_terbit'],
        'tebal'=>$req['tbl_buku'],
        'isbn'=>$req['isbn_buku'],
        'stok_buku'=>$req['stok_buku'],
        'biaya_sewa_harian'=>$req['biaya_buku'],
        
        ]
    );
    return redirect('/buku');
}

public function deleteDataBuku($id){
    DB::table('buku')->where('id', $id)->delete();
    return redirect('/buku');
}

}
