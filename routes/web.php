<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/home', [App\Http\Controllers\ViewController::class,'home']);

Route::get('/buku', [App\Http\Controllers\BukuController::class,'buku']);
Route::get('/mahasiswa', [App\Http\Controllers\MahasiswaController::class,'mahasiswa']);

Route::post('/simpanbuku', [App\Http\Controllers\BukuController::class,'prosesSimpanBuku']);
Route::post('/simpan', [App\Http\Controllers\MahasiswaController::class,'prosesSimpan']);

Route::get('/edit/{id}', [App\Http\Controllers\BukuController::class,'editBuku']);
Route::post('/update', [App\Http\Controllers\BukuController::class,'updateBuku']);
Route::get('/delete/{id}', [App\Http\Controllers\BukuController::class,'deleteDataBuku']);

Route::get('/edit/{id}', [App\Http\Controllers\MahasiswaController::class,'edit']);
Route::post('/update', [App\Http\Controllers\MahasiswaController::class,'update']);
Route::get('/delete/{id}', [App\Http\Controllers\MahasiswaController::class,'deleteData']);


