@extends('layout')
@section('page')
    Edit
@endsection

@section('judul')
    Edit Data Mahasiswa
@endsection

@section('left')
<div id="content">
  <div id="left">
    <h2>Form Data Mahasiswa</h2>
    <form action="/update" method="POST">
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{ $editData->id }}"> <br>
      Nama : <input type="text" name="nama_mhs" value="{{ $editData->nama }}" > <br>
      NIM: <input type="text" name="nim_mhs" value="{{ $editData->nim }}"><br>
      Email : <input type="text" name="email_mhs" value="{{ $editData->email }}" ><br>
      Notelp : <input type="text" name="notelp_mhs" value="{{ $editData->no_telp }}"><br>
      Prodi : <input type="text" name="prodi_mhs"value="{{ $editData->prodi }}"><br>
      Jurusan : <input type="text" name="jurusan_mhs" value="{{ $editData->jurusan }}" ><br>
      Fakultas : <input type="text" name="fakultas_mhs" value="{{ $editData->fakultas }}"><br>
      <input type="submit" value="Simpan">
    </form>
  </div>
  @endsection