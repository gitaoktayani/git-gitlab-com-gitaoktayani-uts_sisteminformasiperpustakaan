@extends('layout')
@section('page')
    Data Mahasiswa
@endsection

@section('judul')
    Data Mahasiswa
@endsection

@section('left')
<div id="content">
  <div id="left">
    <h2>Form Data Mahasiswa</h2>
    <form action="/simpan" method="POST">
      {{ csrf_field() }}
      Nama : <input type="text" name="nama_mhs" > <br>
      NIM: <input type="text" name="nim_mhs" ><br>
      Email : <input type="text" name="email_mhs" ><br>
      Notelp : <input type="text" name="notelp_mhs" ><br>
      Prodi : <input type="text" name="prodi_mhs"><br>
      Jurusan : <input type="text" name="jurusan_mhs" ><br>
      Fakultas : <input type="text" name="fakultas_mhs"><br>
      <input type="submit" value="Simpan">
    </form>
  </div>
  @endsection

@section('right')
<div id="right">
  <div class="box">
    <h2>Data Mahasiswa</h2>
    <table border="1">
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>NIM</th>
        <th>Email</th>
        <th>Notelp</th>
        <th>Prodi</th>
        <th>Jurusan</th>
        <th>Fakultas</th>
        <th>Opsi</th>
      </tr>
      @foreach ($data_mhs as $data)
        <tr>
          <th> {{ $data->id}} </th>
          <th> {{ $data->nama }} </th>
          <th> {{ $data->nim }} </th>
          <th> {{ $data->email }} </th>
          <th> {{ $data->prodi }} </th>
          <th> {{ $data->no_telp }} </th>
          <th> {{ $data->jurusan}} </th>
          <th> {{ $data->fakultas}} </th>
          <td>
            <a href="/edit/{{ $data->id }}"><button style="background-color: 	#87CEEB">Edit</button></a>
            <a href="/delete/{{ $data->id }}"><button style="background-color:#FF4500">Hapus</button></a>
          </td>

        </tr>
      @endforeach
      
    </table>
  </div>
</div>
@endsection