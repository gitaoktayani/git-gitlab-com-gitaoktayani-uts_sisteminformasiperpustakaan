@extends('layout')
@section('page')
    Edit
@endsection

@section('judul')
    Edit Data Mahasiswa
@endsection

@section('left')
<div id="content">
  <div id="left">
    <h2>Form Data Mahasiswa</h2>
    <form action="/update" method="POST">
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{ $editData->id }}"> <br>
      Judul  : <input type="text" name="jdl_buku" value="{{ $editData->judul_buku }}" > <br>
      Pengarang : <input type="text" name="pengarang_buku" value="{{ $editData->pengarang }}"><br>
      Penerbit : <input type="text" name="penerbit_buku" value="{{ $editData->penerbit }}"><br>
      Tahun Terbit : <input type="text" name="thn_terbit" value="{{ $editData->tahun_terbit }}"><br>
      Tebal : <input type="text" name="tbl_buku" value="{{ $editData->tebal }}"><br>
      ISBN : <input type="text" name="isbn_buku" value="{{ $editData->isbn }}"><br>
      Stok Buku : <input type="text" name="stok_buku" value="{{ $editData->stok_buku }}" ><br>
      Biaya Sewa Harian : <input type="text" name="biaya_buku" value="{{ $editData->biaya_sewa_harian }}"><br>
      <input type="submit" value="Simpan">
    </form>
  </div>
  @endsection