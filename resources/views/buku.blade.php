@extends('layout')
@section('page')
    buku
@endsection

@section('judul')
    Data Buku
@endsection

@section('left')
<div id="content">
  <div id="left">
    <h2>Form Data Buku</h2>
    <form action="/simpanbuku" method="POST">
      {{ csrf_field() }}
      Judul  : <input type="text" name="jdl_buku" > <br>
      Pengarang : <input type="text" name="pengarang_buku" ><br>
      Penerbit : <input type="text" name="penerbit_buku" ><br>
      Tahun Terbit : <input type="text" name="thn_terbit" ><br>
      Tebal : <input type="text" name="tbl_buku"><br>
      ISBN : <input type="text" name="isbn_buku" ><br>
      Stok Buku : <input type="text" name="stok_buku" ><br>
      Biaya Sewa Harian : <input type="text" name="biaya_buku"><br>
      <input type="submit" value="Simpan">
    </form>
  </div>
  @endsection

@section('right')
<div id="right">
  <div class="box">
    <h2>Data Buku</h2>
    <table border="1">
      <tr>
        <th>No</th>
        <th>Judul</th>
        <th>Pengarang</th>
        <th>Penerbit</th>
        <th>Tahun Terbit</th>
        <th>Tebal</th>
        <th>ISBN</th>
        <th>Stok Buku</th>
        <th>Biaya Sewa Harian</th>
        <th>Opsi</th>
      </tr>
      @foreach ($data_buku as $data)
        <tr>
          <th> {{ $data->id}} </th>
          <th> {{ $data->judul_buku }} </th>
          <th> {{ $data->pengarang }} </th>
          <th> {{ $data->penerbit }} </th>
          <th> {{ $data->tahun_terbit }} </th>
          <th> {{ $data->tebal }} </th>
          <th> {{ $data->isbn }} </th>
          <th> {{ $data->stok_buku }} </th>
          <th> {{ $data->biaya_sewa_harian }} </th>
          <td>
          <a href="/edit/{{ $data->id }}"><button style="background-color: 	#87CEEB">Edit</button></a>
          <a href="/delete/{{ $data->id }}"><button style="background-color:#FF4500">Hapus</button></a>
          </td>

        </tr>
      @endforeach
    </table>
  </div>
</div>
@endsection